function get_file() { # Arg1: local, arg2: remote, arg3: sudo
  flocal=$1
  fremote=$2
  usesudo=$3
  dlocal=`dirname ${flocal}`
  if [ ! -f ${flocal} ]; then
    if [ "${usesudo}" = true ]; then
      command="sudo mkdir -p ${dlocal} && sudo curl -s -L -o ${flocal} ${fremote}"
    else
      command="mkdir -p ${dlocal} && curl -s -L -o ${flocal} ${fremote}"
    fi
    echo "-- Running: --"
    echo ${command}
    eval $command
    echo "-- Done. --"
  fi
}

function get_compressed_file() { # Arg1: local, arg2: remote, arg3: j/z (bz2/gzip), arg4: command
  flocal=$1
  fremote=$2
  compression=$3
  afterCommand=$4
  if [ ! -e ${flocal} ]; then
    mkdir -p ${flocal}
    command="curl -L -s ${fremote} | tar -x${compression}f - -C ${flocal}"
    echo "-- Running: --"
    echo ${command}
    eval $command
    echo "-- Done. --"
    mv ${flocal}/*/* ${flocal}/
    if [ ! -z "${afterCommand}" ]; then
      eval ${afterCommand}
    fi
  fi
}

function get_deb() { # Arg1: command, arg2: remote
  commandname=$1
  fremote=$2
  if [ -z `which ${commandname}` ]; then
    rm -f /tmp/toinstall.deb
    get_file /tmp/toinstall.deb ${fremote} false
    sudo dpkg --install /tmp/toinstall.deb
  fi
}

if [ `id -u` -eq 0 ]; then
    echo "Do not run as root/via sudo, otherwise local tool installations will fail"
    exit
fi
sudo apt-get update
sudo apt install -y byobu vim xournal git pigz wget keepass2 mono-complete htop curl default-jre libreoffice-java-common openvpn moreutils openconnect chromium-browser libgl1-mesa-glx libegl1-mesa libxcb-xtest0 ibus libc6-i386 gir1.2-vte-2.91 lib32z1 texlive-base texlive-extra-utils texlive-science texlive-lang-german texlive-latex-base texlive-latex-extra texlive-latex-recommended texlive-fonts-extra texlive-formats-extra libcapi20-3 libgsm1 libncurses5 libodbc1 libopenal1 libopencl-clang12 libosmesa6 libpcap0.8 libtinfo5 libcapi20-3:i386 libgsm1:i386 libncurses5:i386 libodbc1:i386 libopenal1:i386 libosmesa6:i386 libpcap0.8:i386 libtinfo5:i386 libldap-dev libldap-dev:i386 zenity libc6:amd64 libc6:i386 libegl1:amd64 libegl1:i386 libgbm1:amd64 libgbm1:i386 libgl1-mesa-dri:amd64 libgl1-mesa-dri:i386 libgl1:amd64 libgl1:i386 steam xterm playonlinux winbind python3-pytest sshpass virtualbox gimp krita
sudo add-apt-repository ppa:appimagelauncher-team/stable
sudo apt update
sudo apt install appimagelauncher
wget -nc https://dl.winehq.org/wine-builds/winehq.key
sudo apt-key add winehq.key
sudo add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ impish main' 
sudo apt-get update
sudo apt install -y --install-recommends winehq-staging
get_file /usr/lib/keepass2/Plugins/KeePassOPT.plgx https://github.com/rookiestyle/keepassotp/releases/latest/download/KeePassOTP.plgx true
get_file ~/tools/passwords.kdbx https://gitlab.com/dabrowskiw/privatefiles/-/raw/master/passwords.kdbx?inline=false false
get_file ~/tools/pcloud wget https://p-lux4.pcloud.com/cBZML0tunZ9jjz8wZZZjeBSi7Z2ZZ6zZkZg7ypVZHZSRZtFZtpZ57ZXHZoHZPFZTVZLzZozZhHZ2zZ2HZhAHEXZ8MbJM1CXRaF2cE9Xs8IN7F7hwxKX/pcloud
rm -f ~/.mozilla/firefox/profiles.ini
chmod ugo+x ~/tools/pcloud
get_compressed_file ~/tools/zotero https://download.zotero.org/client/release/5.0.96.3/Zotero-5.0.96.3_linux-x86_64.tar.bz2 j ""
get_compressed_file ~/tools/jetbrainsToolbox https://download.jetbrains.com/toolbox/jetbrains-toolbox-1.22.10774.tar.gz z "~/tools/jetbrainsToolbox/jetbrains-toolbox"
rm -f ~/.config/autostart/jetbrains-toolbox.desktop
get_compressed_file ~/tools/eclipse/installer https://ftp.snt.utwente.nl/pub/software/eclipse/oomph/epp/2021-09/R/eclipse-inst-jre-linux64.tar.gz z "~/tools/eclipse/installer/eclipse-inst"
get_file ~/tools/joplin https://github.com/laurent22/joplin/releases/download/v2.5.12/Joplin-2.5.12.AppImage
chmod ugo+x ~/tools/joplin
get_deb zoom https://htw-berlin.zoom.us/client/latest/zoom_amd64.deb
#get_deb crossover https://media.codeweavers.com/pub/crossover/cxlinux/demo/crossover_21.1.0-1.deb
get_deb steam https://repo.steampowered.com/steam/archive/precise/steam_latest.deb

sudo add-apt-repository ppa:mozillateam/ppa
sudo apt update
sudo apt install thunderbird firefox

sudo apt upgrade

# Current ModemManager install, for FCC unlocking of Quectel EM120 in Thinkpad P14s. Needs restart.
sudo apt install libmbim-utils libqmi-utils gettext

cd ~/tools

git clone https://gitlab.freedesktop.org/mobile-broadband/libmbim.git
cd libmbim
meson setup build --prefix=/usr
sudo ninja -C build install

git clone --depth 1 https://gitlab.freedesktop.org/mobile-broadband/libqrtr-glib.git
cd libqrtr-glib
meson setup build --prefix=/usr
sudo ninja -C build install

git clone https://gitlab.freedesktop.org/mobile-broadband/libqmi.git
cd libqmi
meson setup build --prefix=/usr
sudo ninja -C build install

git clone https://gitlab.freedesktop.org/mobile-broadband/ModemManager.git
cd ModemManager
meson setup build --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib/x86_64-linux-gnu/ -Dmbim=true -Dqmi=true -Dqrtr=true --buildtype=release
sudo ninja -C build install
sudo ln -sft /etc/ModemManager/fcc-unlock.d/ /usr/share/ModemManager/fcc-unlock.available.d/*

sudo apt install libgtk-3-dev libgdbm-dev ofono ofono-dev libgtkspell3-3-0 libgtkspell3-3-dev libayatana-appindicator3-1 libayatana-appindicator3-dev po4a itstool

wget https://deac-fra.dl.sourceforge.net/project/modem-manager-gui/modem-manager-gui-0.0.20.tar.gz
tar -xvzf modem-manager-gui-0.0.20.tar.gz
cd modem-manager-gui
./configure
make -j 8
sudo make install

# https://www.davidrevoy.com/article331/setup-huion-giano-wh1409-tablet-on-linux-mint-18-1-ubuntu-16-04
sudo apt install -y digimend-dkms
sudo cp 50-huion.conf /usr/share/X11/xorg.conf.d/

sudo add-apt-repository ppa:apandada1/xournalpp-stable
sudo apt update
sudo apt install -y xournalpp

## Thinkfan: Using nvml prevents the GPU from switching to suspend power state, causing battery drain and high temperatures.

#echo 'options thinkpad_acpi fan_control=1' | sudo tee /lib/modprobe.d/thinkpad_acpi.conf
#
#sudo apt install thinkfan
#
#sudo cp thinkfan.conf /etc/thinkfan.conf
#
#echo 'THINKFAN_ARGS="-c /etc/thinkfan.conf"' | sudo tee -a /etc/default/thinkfan
#
#sudo systemctl enable thinkfan
#
#sudo systemctl start thinkfan
